<?php
include_once 'layout/navbar.php';
if (!isset($_SESSION['id']))
    header('location:SignIn.php')
?>

<div class="container">

    <?php
    include 'src/Database.php';
    $database=new Database();

    $result=$database->get('users','*','status=0');

    ?>
    <table class="table table-bordered" border="2" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Full name</th>
            <th>User ID</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Pate Number</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $user){ ?>
            <tr >
                <td><?php echo $user->id ?></td>
                <td><?php echo $user->full_name ?></td>
                <td><?php echo $user->user_id ?></td>
                <td><?php echo $user->email ?></td>
                <td><?php echo $user->phone ?></td>
                <td><?php echo $user->plat_number ?></td>
                <?php if ($user->type ==0){ ?>
                    <td><?php echo 'Student' ?></td>
                <?php }?>

                <?php if ($user->type ==1){ ?>
                    <td><?php echo 'Employee' ?></td>
                <?php }?>
                <td>
                    <div >
                        <a href="src/approve-user.php?id=<?php echo $user->id?>" type="button" class="btn btn-primary"><i class="fa fa-trash"></i>Approve</a>
                        <a href="src/delete-user.php?id=<?php echo $user->id?>" type="button" class="btn btn-danger"><i class="fa fa-trash"></i>Delete</a>
                    </div>
                </td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>
