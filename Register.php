<!DOCTYPE html>
<html lang="en">
<?php

include_once 'layout/navbar.php'
?>
<body style="background-image:url('cars2.jpg');">


<?php if (isset($_GET['success'])){?>
<div class="alert alert-success">
    <strong>Success</strong> Registered successfully your application will be reviewed by admin
</div>
<?php }?>
<?php if (isset($_SESSION['student_error']) || isset($_SESSION['employee_error'])){
    ?>
<div class="alert alert-danger">
    <strong>failed to register</strong>
</div>
<?php }?>
<div class="text-center" style="height: 260px;vertical-align: middle;display: block;margin-top: 290px;">
    <h1  style="text-shadow:1px 1px 0 #444">WELCOME TO OUR SITE</h1>
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#student">Register as student</button>
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#employee">Register as employee</button>
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#visitor">Register as visitor</button>
</div>

<div class="modal fade" id="student" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Register as student</h4>
            </div>
            <div class="modal-body">

                <form name="my-form" onsubmit="return validform()" action="src/student_register.php" method="post">
                    <div class="form-group row">
                        <?php if (isset($_SESSION['student_error']['full_name'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['student_error']['full_name']?>
                            </div>
                        <?php }?>
                        <label for="full_name" class="col-md-4 col-form-label text-md-right">Full Name</label>
                        <div class="col-md-6">
                            <input type="text" id="full_name" class="form-control" name="full_name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['student_error']['email'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['student_error']['email']?>
                            </div>
                        <?php }?>
                        <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                        <div class="col-md-6">
                            <input type="email" id="email_address" class="form-control" name="email">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['student_error']['stdId'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['student_error']['stdId']?>
                            </div>
                        <?php }?>
                        <label for="ID_number" class="col-md-4 col-form-label text-md-right">Student ID</label>
                        <div class="col-md-6">
                            <input type="text" id="ID_number" class="form-control" name="student_id">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['student_error']['phone'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['student_error']['phone']?>
                            </div>
                        <?php }?>
                        <label for="phone_number" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                        <div class="col-md-6">
                            <input type="text" id="phone_number" name="phone" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['student_error']['plate'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['student_error']['plate']?>
                            </div>
                        <?php }?>
                        <label for="plate_number" class="col-md-4 col-form-label text-md-right">Car plate number</label>
                        <div class="col-md-6">
                            <input type="text" id="plate_number" name="plate" class="form-control">
                        </div>
                    </div>



            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </div>
            </form>
        </div>
    </div>
</div>





<div class="modal fade" id="employee" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Register as employee</h4>
            </div>
            <div class="modal-body">

                <form name="my-form" onsubmit="return validform()" action="src/employee_register.php" method="post">
                    <div class="form-group row">
                        <?php if (isset($_SESSION['employee_error']['full_name'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['employee_error']['full_name']?>
                            </div>
                        <?php }?>
                        <label for="full_name" class="col-md-4 col-form-label text-md-right">Full Name</label>
                        <div class="col-md-6">
                            <input type="text" id="full_name" class="form-control" name="full_name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['employee_error']['email'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['employee_error']['email']?>
                            </div>
                        <?php }?>
                        <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                        <div class="col-md-6">
                            <input type="email" id="email_address" class="form-control" name="email">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['employee_error']['stdId'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['employee_error']['stdId']?>
                            </div>
                        <?php }?>
                        <label for="ID_number" class="col-md-4 col-form-label text-md-right">Employee ID</label>
                        <div class="col-md-6">
                            <input type="text" id="ID_number" class="form-control" name="student_id">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['employee_error']['phone'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['employee_error']['phone']?>
                            </div>
                        <?php }?>
                        <label for="phone_number" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                        <div class="col-md-6">
                            <input type="text" id="phone_number" name="phone" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['employee_error']['plate'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['employee_error']['plate']?>
                            </div>
                        <?php }?>
                        <label for="plate_number" class="col-md-4 col-form-label text-md-right">Car plate number</label>
                        <div class="col-md-6">
                            <input type="text" id="plate_number" name="plate" class="form-control">
                        </div>
                    </div>



            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </div>
            </form>
        </div>
    </div>
</div>




<div class="modal fade" id="visitor" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">visitor as employee</h4>
            </div>
            <div class="modal-body">

                <form name="my-form" onsubmit="return validform()" action="src/visitor_register.php" method="post">
                    <div class="form-group row">
                        <?php if (isset($_SESSION['visitor_error']['full_name'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['visitor_error']['full_name']?>
                            </div>
                        <?php }?>
                        <label for="full_name" class="col-md-4 col-form-label text-md-right">Full Name</label>
                        <div class="col-md-6">
                            <input type="text" id="full_name" class="form-control" name="full_name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['visitor_error']['email'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['visitor_error']['email']?>
                            </div>
                        <?php }?>
                        <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                        <div class="col-md-6">
                            <input type="email" id="email_address" class="form-control" name="email">
                        </div>
                    </div>


                    <div class="form-group row">
                        <?php if (isset($_SESSION['visitor_error']['phone'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['visitor_error']['phone']?>
                            </div>
                        <?php }?>
                        <label for="phone_number" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                        <div class="col-md-6">
                            <input type="text" id="phone_number" name="phone" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['visitor_error']['from'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['visitor_error']['from']?>
                            </div>
                        <?php }?>
                        <label for="phone_number" class="col-md-4 col-form-label text-md-right">Enter date</label>
                        <div class="col-md-6">
                            <input type="datetime-local" id="phone_number" name="from" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['visitor_error']['to'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['visitor_error']['to']?>
                            </div>
                        <?php }?>
                        <label for="phone_number" class="col-md-4 col-form-label text-md-right">Leave date</label>
                        <div class="col-md-6">
                            <input type="datetime-local" id="phone_number" name="to" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['visitor_error']['plate'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['visitor_error']['plate']?>
                            </div>
                        <?php }?>
                        <label for="plate_number" class="col-md-4 col-form-label text-md-right">Car plate number</label>
                        <div class="col-md-6">
                            <input type="text" id="plate_number" name="plate" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php if (isset($_SESSION['visitor_error']['reasons'])){?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $_SESSION['visitor_error']['reasons']?>
                            </div>
                        <?php }?>
                        <label for="plate_number" class="col-md-4 col-form-label text-md-right">Reasons</label>
                        <div class="col-md-6">
                            <textarea id="plate_number" name="reasons" class="form-control"></textarea>
                        </div>
                    </div>



            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>







<?php
/**
 * Created by PhpStorm.
 * User: Eyad
 * Date: 04/22/2019
 * Time: 04:00 م
 */