<?php
include_once 'layout/navbar.php';
if (!isset($_SESSION['id']))
    header('location:SignIn.php')
?>

<div class="container">

    <?php
    include 'src/Database.php';
    $database=new Database();

    $result=$database->get('visitor','*','status=0');
    ?>
    <table class="table table-bordered" border="2" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Full name</th>
            <th>Email</th>
            <th>Plat number</th>
            <th>phone</th>
            <th>Enter data</th>
            <th>Leave date</th>
            <th>reasons</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $user){ ?>
            <tr >
                <td><?php echo $user->id ?></td>
                <td><?php echo $user->full_name ?></td>
                <td><?php echo $user->email ?></td>
                <td><?php echo $user->plat_number ?></td>
                <td><?php echo $user->phone ?></td>
                <td><?php echo $user->enter_date ?></td>
                <td><?php echo $user->leave_date ?></td>
                <td><?php echo $user->reasons ?></td>
                <td>
                    <div >
                        <a href="src/approve-visitors.php?id=<?php echo $user->id?>" type="button"  class="btn btn-primary edit-student-btn" data-student-id="@cl.StudentID"><i class="fa fa-edit"></i>Approve</a>
                        <a href="src/delete-victors.php?id=<?php echo $user->id?>" type="button" class="btn btn-danger"><i class="fa fa-trash"></i>Delete</a>
                    </div>
                </td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>
