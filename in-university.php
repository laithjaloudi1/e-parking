<?php
include_once 'layout/navbar.php';
if (!isset($_SESSION['id']))
    header('location:SignIn.php')
?>

<div class="container">

    <?php
    include 'src/Database.php';
    $database=new Database();
    $query="select email,full_name,plat_number,phone,'visitor' AS \"type\" from visitor where in_university=1
union all
select email,full_name,plat_number,phone,'users' AS \"type\" from users where in_university=1";
    $result=$database->inUniversity($query);
    ?>
    <table class="table table-bordered" border="2" width="100%">
        <thead>
        <tr>
            <th>Full name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Pate Number</th>
            <th>Type</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $user){ ?>
            <tr >
                <td><?php echo $user->full_name ?></td>
                <td><?php echo $user->email ?></td>
                <td><?php echo $user->phone ?></td>
                <td><?php echo $user->plat_number ?></td>
                <td><?php echo $user->type ?></td>

            </tr>
        <?php }?>
        </tbody>
    </table>
</div>
