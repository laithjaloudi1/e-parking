
<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body style="background-image:url('../cars2.jpg');">

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">E-parking gate</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Approve List
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="../user-approved.php">users approved</a></li>
                    <li><a href="../visitors-approved.php">visitors approved</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">requests
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="../user-request.php">users requests</a></li>
                    <li><a href="../visitors-request.php">visitors requests</a></li>
                </ul>
            </li>
            <li><a href="../in-university.php">Current cars at the university</a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right" >
            <li>
                <a  href="#">
                    <?php
                    echo "welcome " . $_SESSION['fname']; ?></a>
            </li>

            <li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
            <?php if (!isset($_SESSION['id'])) {?>
            <li class="active"><a href="#">Log in</a></li>
            <li><a href="Register.php">Register</a></li>
            <?php }?>


        </ul>
    </div>



</nav>


</body>
</html>