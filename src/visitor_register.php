<?php
/**
 * Created by PhpStorm.
 * User: laith
 * Date: 26/04/19
 * Time: 09:38 م
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
$fullName=$_POST['full_name'];
$email=$_POST['email'];
$phone=$_POST['phone'];
$carPlate=$_POST['plate'];
$from=$_POST['from'];
$to=$_POST['to'];
$reasons=$_POST['reasons'];

$errors=[];
include 'Database.php';

$database=new Database();


if(empty($fullName)){
    $errors['full_name']='full name is required';
}

if (empty($email))
    $errors['email']='email is required';


if (empty($phone))
    $errors['phone']='Phone is required';

if (empty($carPlate))
    $errors['plate']='plate number is required';

if (!filter_var($email,FILTER_VALIDATE_EMAIL))
    $errors['email']='Invalid email format';

if (empty($from))
    $errors['from']='enter date is required ';

if (empty($to))
    $errors['to']='leave date is required ';

if (empty($reasons))
    $errors['reasons']='reasons is required ';


if (count($errors)==0)
{


    if (count($errors) == 0)
    {
        $att=[
            'full_name'=>"'$fullName'",
            'email'=>"'$email'",
            'phone'=>$phone,
            'plat_number'=>"'$carPlate'",
            'enter_date'=>"'$from'",
            'leave_date'=>"'$to'",
            'reasons'=>"'$reasons'",
        ];
        $result= $database->insert('visitor',$att);
        unset($_SESSION['visitor_error']);
        header("location: ../Register.php?success=1");
    }else{
        $_SESSION['visitor_error']=[];
        $_SESSION['visitor_error']=$errors;
        header("location: ../Register.php");

    }
}
else{
    $_SESSION['visitor_error']=[];
    $_SESSION['visitor_error']=$errors;
    header("location: ../Register.php");
}




