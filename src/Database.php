<?php

class Database
{

    private $conn;
    public function __construct()
    {
        $servername='localhost';
        $username='root';
        $password='1234';
        $dbname='e_parking';

        try {
            $connection = new PDO("mysql:host=$servername;dbname=e_parking", $username, $password);
            // set the PDO error mode to exception
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn=$connection;
        }
        catch(PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function get($table='',$select=[],$filter=[])
    {
        $query="SELECT {$select} FROM {$table} where {$filter}";
        $result=$this->conn->query($query);

        return $result->fetchAll(PDO::FETCH_OBJ);
    }

    public function first($table='',$select=[],$filter=[])
    {
        $query="SELECT {$select} FROM {$table} where {$filter}";
        $result=$this->conn->query($query);
        if ($result->rowCount()>0)
        return $result->fetchObject();

        return [];
    }

    public function insert($table='', $attribute=[])
    {
        $columns = implode(", ",array_keys($attribute));
        $values= implode(", ",array_values($attribute));
        $query="INSERT INTO {$table} ($columns) VALUES ($values)";
        if ($this->conn->query($query) === TRUE) {
           return 1;
        }
        return 0;
    }

    public function delete($table='',$id)
    {
        $query="DELETE FROM {$table} where id={$id}";
        $result=$this->conn->query($query);
        if($result)
            return true;

        return false;
    }

    public function approve($query)
    {
        $result=$this->conn->query($query);
        if ($result->rowCount()>0)
            return 1;

        return 0;
    }

    public function inUniversity($query)
    {
        $result=$this->conn->query($query);
        return $result->fetchAll(PDO::FETCH_OBJ);
    }

}