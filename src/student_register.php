<?php
session_start();
$fullName=$_POST['full_name'];
$email=$_POST['email'];
$stdId=$_POST['student_id'];
$phone=$_POST['phone'];
$carPlate=$_POST['plate'];

$errors=[];
include 'Database.php';

$database=new Database();


    if(empty($fullName)){
        $errors['full_name']='full name is required';
    }

    if (empty($email))
        $errors['email']='email is required';

    if (empty($stdId))
        $errors['stdId']='Student ID is required';

    if (!is_numeric($stdId))
        $errors['stdId']='Student ID is must be an number';

    if (empty($phone))
        $errors['phone']='Phone is required';

    if (empty($carPlate))
        $errors['plate']='plate number is required';

    if (!filter_var($email,FILTER_VALIDATE_EMAIL))
        $errors['email']='Invalid email format';

    if (strlen($stdId)>9)
        $errors['stdId']='student number is longer than 9 number';

    if (strlen($stdId)<9)
    $errors['stdId']='student number is shorter than 9 number';


    if (count($errors)==0)
    {

        $user_id=$database->first('users','*',"user_id={$stdId}");
        $emailValidate=$database->first('users','*',"email='$email'");
        $plateValidate=$database->first('users','*',"plat_number='$carPlate'");

        if (count($user_id) > 0)
            $errors['userId']='Student ID exist';

        if (count($emailValidate)>0)
            $errors['email']='email is taken';

        if (count($plateValidate)>0)
            $errors['plate']='plate number is taken';

        if (count($errors) == 0)
        {
        $att=[
            'full_name'=>"'$fullName'",
            'user_id'=>$stdId,
            'email'=>"'$email'",
            'phone'=>$phone,
            'plat_number'=>"'$carPlate'",
            'type'=>0
         ];
            $result= $database->insert('users',$att);
            unset($_SESSION['student_error']);
            header("location: ../Register.php?success=1");
        }else{
            $_SESSION['student_error']=[];
            $_SESSION['student_error']=$errors;
            header("location: ../Register.php");

        }
    }
    else{
        $_SESSION['student_error']=[];
        $_SESSION['student_error']=$errors;
        header("location: ../Register.php");
    }




